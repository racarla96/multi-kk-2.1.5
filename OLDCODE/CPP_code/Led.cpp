// Led.cpp
#include "Led.h"

Led::Led() {
  LED_DDR |= _BV(LED); // LED Config Output
}

void Led::On() {
  LED_PORT |= _BV(LED); // LED On
}

void Led::Off() {
  LED_PORT &= ~_BV(LED); // LED Off
}
