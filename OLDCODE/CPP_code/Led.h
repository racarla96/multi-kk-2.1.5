// Led.h
#ifndef _LED_H_
#define _LED_H_

#include "hwPinoutBoardDef.h"
#include <avr/io.h>

class Led
{
public:
	Led();
	void On();
	void Off();
};

#endif
