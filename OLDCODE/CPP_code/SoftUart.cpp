// SoftUart.cpp
#include "SoftUart.h"

void SoftUart::init() {
  UART_TX_DDR |= _BV(UART_TX); // UART Debug TX Config Ouput
}

void SoftUart::write(uint8_t b){
  cli();  // turn off interrupts for a clean txmit

  // Write the start bit
  UART_TX_PORT &= ~_BV(UART_TX); // UART_TX LOW

  _delay_loop_2(UART_DELAY);

  // Write each of the 8 bits
  for (uint8_t i = 8; i > 0; --i)
  {
    if (b & 1) // choose bit
      UART_TX_PORT |= _BV(UART_TX); // UART_TX HIGH
    else
      UART_TX_PORT &= ~_BV(UART_TX); // UART_TX LOW

    _delay_loop_2(UART_DELAY);
    b >>= 1;
  }

  // restore pin to natural state
  UART_TX_PORT |= _BV(UART_TX); // UART_TX HIGH

  sei(); // the same

  _delay_loop_2(UART_DELAY);
}

void SoftUart::print(String w){
  for(int i = 0; i < w.length(); i++){
    this.write(w[i]);
  }
}
