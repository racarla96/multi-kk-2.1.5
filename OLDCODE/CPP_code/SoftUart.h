// SoftUart.h
#ifndef _SOFTUART_H_
#define _SOFTUART_H_

#include "hwPinoutBoardDef.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string>
#include <stdint.h>
#include <util/delay.h>

#define UART_BAUDRATE 115200
#define UART_BIT_DELAY ((F_CPU / UART_BAUDRATE) / 4)

#if UART_BIT_DELAY > 3
  #define UART_DELAY (UART_BIT_DELAY - 3)
#else
  #define UART_DELAY 1
#endif

class SoftUart
{
public:
  void init();
  void write(uint8_t b);
  void print(String w);
};

#endif
