// hwPinoutBoardDef.h
#ifndef _HWPINOUTBOARDDEF_H_
#define _HWPINOUTBOARDDEF_H_

/*
HARDWARE PINOUT BOARD DEFINITION

PA0 - ¿? +5V
PA1 - ¿? MPU Interrupt Input Pin | Battery Voltage
PA2 - ¿? None
PA3 - ¿? None
PA4 - ¿? Output Pin 4
PA5 - ¿? Output Pin 5
PA6 - ¿? None
PA7 - ¿? None

PB0 - Input Pin AUX / USE AS UART TX PORT
PB1 - ¿? Buzzer
PB2 - ¿? Input Pin RUDDER
PB3 - LED RED D2
PB4 - ¿? Button X
PB5 - ¿? Button X
PB6 - ¿? Button X
PB7 - ¿? Button X

PC0 - ¿? SCL IMU
PC1 - ¿? SDA IMU
PC2 - ¿? Output Pin 3
PC3 - ¿? Output Pin 4
PC4 - ¿? Output Pin 2
PC5 - ¿? Output Pin 7
PC6 - ¿? Output Pin 1
PC7 - ¿? Output Pin 8

PD0 - ¿? Input Pin THROTTLE
PD1 - ¿? LCD Screen Pin SI
PD2 - ¿? Input Pin ELEVATOR
PD3 - ¿? Input Pin AILERON
PD4 - ¿? LCD Screen Pin SCL
PD5 - ¿? LCD Screen Pin CS1
PD6 - ¿? LCD Screen Pin RES
PD7 - ¿? LCD Screen Pin A0
*/

// Crystal Frequency MCU
#define F_CPU 20000000L

// LED RED D2
#define LED_DDR DDRB
#define LED_PORT PORTB
#define LED 3

// UART DEBUG TX by Software
// 115200 8N1
#define UART_TX_DDR DDRB
#define UART_TX_PORT PORTB
#define UART_TX 0

//
#define MPU_INTERRUPT_DDR DDRA
#define MPU_INTERRUPT_PORT PORTA
#define MPU_INTERRUPT 1

/*
ST7565 LCD Screen
- Backlight always On
*/

#define SID_DDR DDRD
#define SID_PIN PIND
#define SID_PORT PORTD
#define SID 1

#define SCLK_DDR DDRD
#define SCLK_PIN PIND
#define SCLK_PORT PORTD
#define SCLK 4

#define A0_DDR DDRD
#define A0_PIN PIND
#define A0_PORT PORTD
#define A0 7

#define RST_DDR DDRD
#define RST_PIN PIND
#define RST_PORT PORTD
#define RST 6

#define CS_DDR DDRD
#define CS_PIN PIND
#define CS_PORT PORTD
#define CS 5

#endif
