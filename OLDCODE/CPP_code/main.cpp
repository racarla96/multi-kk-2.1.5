#include "config.h"

// ================================================================
// ===               INTERRUPT DETECTION ROUTINE                ===
// ================================================================

volatile bool mpuInterrupt = false;     // indicates whether MPU interrupt pin has gone high
void dmpDataReady() {
  mpuInterrupt = true;
}

void config_interrupts(){
	MPU_INTERRUPT_DDR &= ~_BV(MPU_INTERRUPT);

}

void led_init_sequence(Led led){
	// LED Initial sequence
	led.Off();
	for(int i=0; i < 2; i++){
		led.On();
		_delay_ms(500);
		led.Off();
		_delay_ms(500);
	}
}

int main(void) {

	init_board();

	Led led;

	SoftUart uart;
	uart.init();

	MPU6050 mpu;
	Fastwire::setup(400, true);
	mpu.initialize();

	config_interrupts();

	uart.print("Initializing I2C devices...");
	led_init_sequence(led);

	if(mpu.testConnection()) {
		uart.print("MPU6050 connection successful");
		led.On();
	}
 	else {
		uart.print("MPU6050 connection failed");
		led.Off();
	}
	_delay_ms(500);

	uart.print("Initializing DMP...");
	devStatus = mpu.dmpInitialize();

	// make sure it worked (returns 0 if so)
	if (devStatus == 0) {
		// Calibration Time: generate offsets and calibrate our MPU6050
		mpu.CalibrateAccel(6);
		mpu.CalibrateGyro(6);
		// turn on the DMP, now that it's ready
		uart.print("Enabling DMP...");
		mpu.setDMPEnabled(true);

		// enable Arduino interrupt detection
    Serial.print(F("Enabling interrupt detection (Arduino external interrupt "));
    Serial.print(digitalPinToInterrupt(INTERRUPT_PIN));
    Serial.println(F(")..."));
    attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), dmpDataReady, RISING);
    mpuIntStatus = mpu.getIntStatus();

    // set our DMP Ready flag so the main loop() function knows it's okay to use it
    Serial.println(F("DMP ready! Waiting for first interrupt..."));
    dmpReady = true;

    // get expected DMP packet size for later comparison
    packetSize = mpu.dmpGetFIFOPacketSize();
	} else {
		// ERROR!
		// 1 = initial memory load failed
		// 2 = DMP configuration updates failed
		// (if it's going to break, usually the code will be 1)
		uart.print("DMP Initialization failed (code ");
		uart.write(devStatus);
		uart.print(")");
		while(1);
	}

// UART TEST
/*
for(int i = 0; i < 10; i++) {
	led.On();
	for(uint8_t c = 65; c < 91; c++) SoftUart_write(c);
	for(uint8_t c = 97; c < 123; c++) SoftUart_write(c);
	SoftUart_write(13);
	SoftUart_write(10);
	led.Off();
	_delay_ms(500);
}
*/

// LED TEST
/*
	for(int i=0; i < 10; i++){
		led.On();
		_delay_ms(500);
		led.Off();
		_delay_ms(500);
	}
*/

	return 0;
}
