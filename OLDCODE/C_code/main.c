#include "hwPinoutBoardDef.h"

#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include <stdint.h>

#include <stdio.h> // Support printf
#include <avr/interrupt.h> // Support printf

#define UART_BAUDRATE 115200
#define UART_BIT_DELAY ((F_CPU / UART_BAUDRATE) / 4)

#if UART_BIT_DELAY > 3
  #define UART_DELAY (UART_BIT_DELAY - 3)
#else
  #define UART_DELAY 1
#endif

void write_uart(uint8_t b);


static int uart_putchar_printf(char c, FILE *stream); // Support printf
static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar_printf, NULL, _FDEV_SETUP_WRITE); // Support printf

// Support printf
static int uart_putchar_printf(char var, FILE *stream){
  if (var == '\n') write_uart('\r');
  write_uart(var);
  return 0;
}

int main (void) {

  LED_DDR |= _BV(LED); // LED Config Ouput
  UART_TX_DDR |= _BV(UART_TX); // UART Debug TX Config Ouput

  stdout = &mystdout; // Support printf

  LED_PORT |= _BV(LED); // LED On

  // Support printf
  printf("Hello world!!!\n");
  printf("Number Test %d!!!\n", 10);

  LED_PORT &= ~_BV(LED); // LED Off

/*
  for(int i = 0; i < 5; i++) {
    LED_PORT |= _BV(LED); // LED On
    for(uint8_t c = 65; c < 91; c++) write_uart(c);
    for(uint8_t c = 97; c < 123; c++) write_uart(c);
    write_uart(13);
    write_uart(10);
    LED_PORT &= ~_BV(LED); // LED Off
    _delay_ms(500);
  }
*/

/*
    LED_DDR |= _BV(LED); // LED Config Ouput

    for(int i=0; i < 5; i++){
    LED_PORT |= _BV(LED); // LED On
    _delay_ms(500);
    LED_PORT &= ~_BV(LED); // LED Off
    _delay_ms(500);
  }
*/

    while(1);

    return (0);
}

void write_uart(uint8_t b){

  cli();  // turn off interrupts for a clean txmit

  // Write the start bit
  UART_TX_PORT &= ~_BV(UART_TX); // UART_TX LOW

  _delay_loop_2(UART_DELAY);

  // Write each of the 8 bits
  for (uint8_t i = 8; i > 0; --i)
  {
    if (b & 1) // choose bit
      UART_TX_PORT |= _BV(UART_TX); // UART_TX HIGH
    else
      UART_TX_PORT &= ~_BV(UART_TX); // UART_TX LOW

    _delay_loop_2(UART_DELAY);
    b >>= 1;
  }

  // restore pin to natural state
  UART_TX_PORT |= _BV(UART_TX); // UART_TX HIGH

  sei(); // the same

  _delay_loop_2(UART_DELAY);
}
