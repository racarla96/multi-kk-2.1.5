// SetupHardware.h
#ifndef _HWPINOUTBOARDDEF_H_
#define _HWPINOUTBOARDDEF_H_

/*
;       76543210	;set port directions
ldi t,0b00110010	;output5, output6
out ddra,t

;       76543210
ldi t,0b00001010
out ddrb,t

;       76543210
ldi t,0b11111100	;scl, sda, output 1-8
out ddrc,t

;       76543210
ldi t,0b11110010
out ddrd,t
*/

#endif
