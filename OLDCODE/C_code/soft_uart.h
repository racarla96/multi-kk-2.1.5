// soft_uart.h
#ifndef _SOFT_UART_H_
#define _SOFT_UART_H_

// Default config
// 115200 8N1
 #define baudrate 115200

uint16_t bit_delay = (F_CPU / baudrate) / 4;
uint16_t _tx_delay = subtract_cap(bit_delay, 15 / 4);

#endif
