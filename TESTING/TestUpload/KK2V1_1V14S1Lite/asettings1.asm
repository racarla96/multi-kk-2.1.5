.def Item		= r17

AdvancedSettings1:

asux11:	call LcdClear
	
	lrv PixelType, 1
	lrv FontSelector, f6x8

	lrv X1,0		;board offset
	lrv Y1,1
	mPrintString asux1
	ldz eeBoardOffset
	call GetEeVariable8 
	cpi xl,0
	brne asux18a
	mPrintString asux25
	rjmp asux19
asux18a:	
	cpi xl,1
	brne asux18b
	mPrintString asux26
	rjmp asux19
asux18b:
	mPrintString asux27
asux19:
	lrv X1,0		;Spin on Arm
	lrv Y1,10
	mPrintString asux4
	call GetEeVariable8 
	brflagfalse xl, asux30
	mPrintString asux28
	rjmp asux32
asux30:	mPrintString asux29
asux32:

	lrv X1,0		;SS Gimbal
	lrv Y1,19
	mPrintString asux5
	call GetEeVariable8 
	brflagfalse xl, asux33
	mPrintString asux28
	rjmp asux34
asux33:	mPrintString asux29
asux34:

	lrv X1,0		;SS Gimbal
	lrv Y1,28
	mPrintString asux5a
	call GetEeVariable8 
	brflagfalse xl, asux35
	mPrintString asux28
	rjmp asux36
asux35:	mPrintString asux29

asux36:

	;footer
	lrv X1, 0
	lrv Y1, 57
	mPrintString asux6

	;print selector
	ldzarray asux7*2, 4, Item
	lpm t, z+
	sts X1, t
	lpm t, z+
	sts Y1, t
	lpm t, z+
	sts X2, t
	lpm t, z
	sts Y2, t
	lrv PixelType, 0
	call HilightRectangle

	call LcdUpdate

	call GetButtonsBlocking

	cpi t, 0x08		;BACK?
	brne asux8
	ret	

asux8:	cpi t, 0x04		;PREV?
	brne asux9	
	dec Item
	brpl asux10
	ldi Item, 4
asux10:	rjmp asux11	

asux9:	cpi t, 0x02		;NEXT?
	brne asux12
	inc Item
	cpi item, 4
	brne asux13
	ldi Item, 0
asux13:	rjmp asux11	

asux12:	cpi t, 0x01		;CHANGE?
	brne asux14

	cpi Item,0
	brne asux12a
	lds		t,BoardOffset
	inc		t
	andi	t,0b00000011
	cpi		t,0b00000011
	brne    asux12b
	ldi		t,0
asux12b:
	sts		BoardOffset,t
	mov		xl,t
	ldz eeBoardOffset
	call StoreEeVariable8
	rjmp asux14
asux12a:
	ldzarray eeBoardOffset, 1, Item	;toggle flag
	call GetEeVariable8
	ldi t, 0x80
	eor xl, t
	ldzarray eeBoardOffset, 1, Item
	call StoreEeVariable8

asux14:	rjmp asux11




asux1:	.db "Board Offset   : ", 0
asux4:	.db "Spin on Arm    : ", 0
asux5:	.db "SS Gimbal      : ", 0
asux5a: .db "Gimbal Control : ", 0
asux6:	.db "BACK PREV NEXT CHANGE", 0
asux28:	.db "Yes",0
asux29:	.db "No",0,0
asux25:	.db "-45",0
asux26:	.db "0",0
asux27:	.db "+45",0



asux7:	.db 100, 0, 122, 9
	.db 100, 9, 122, 18
	.db 100, 18, 122, 27
	.db 100, 27, 122, 36


.undef Item

