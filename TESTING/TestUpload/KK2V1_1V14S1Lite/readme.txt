HW Version 2.1
SW Version 1.14S1
Steveis

THIS IS THE BEGINNER VERSION WHICH LOOKS AND FEELS LIKE A KK2.0

Changes sice HK V1.6 for KK2.1

Default MPU6050 settings now 500 deg/sec and 4g which are close to KK2.0 so P&I and Stick Scale values should be close to those used on KK2.0

If you fly aggressive acro, you will need to upgrade the firmware to the pro version and select a gyro rate of 2000 deg per second.

Warnings:

i)  Firmware will reset all settings and you will need to select a motor layout.

Critical bug(s) corrected: 

i)	Corrected pin assignment for Output 5 and Output 6
ii)	Initialisation settings didn't get written to the MPU6050 so it was stuck on 250 deg/sec and 2g

Minor bug(s) corrected: 

i) Updated KK1_6_MPU6050 to remove unused code for menu button press (thanks RC911)
ii) Updated KK1_6_MPU6050 to correct Meny code to disable OCR1A and B interrupt (thanks RC911)
iii) Changed/Corrected I2C routines so they actually work now
iv) Tidied up I2C routine for burst reading of sensor data
v) More accurate battery voltage - adjusted to read the same as my KK2.0 (thanks HappySundays) - updated again from V1.14S1 after testing on three KK2.1s.
vi) Corrected low voltage alarm calculation.
vii) Correct constants are now used in imu.asm and trigonometry.asm depending on acc and gyro setting so no problems with self level.
viii) Stick Scaling does not need to be modified (except for fine tuning) when changing gyro rate
ix) RC911 bug fix in the Number Editor (original firmware) that allowed setting a value to zero (CLR) when the lower limit was higher.
x) RC911 bug fix in the original firmware that kept the "Link Roll Pitch" flag from being updated until the user returned to the SAFE screen.
xi) From V1.14S1, small bug fixed in imu.asm for 0.5G to 1.5G test (thanks jmdhuse).

Additions: 

i) Debug Menu (added back) plus some extra values displayed
ii) Version Menu
iii)
iv) 
v) MPU6050 Temperature shown on SAFE screen
vi) Receiver Sliders Menu that shows you the msec value and sliders for the receiver inputs - for info, not calibrated - includes CH6 & CH7 for CPPM and Satellites
vii) Output Sliders Menu that shows you the msec value and sliders for the motor/servo outputs - this is for info as it is not calibrated 
viii) Acc Bubble Level Menu for accelerometers
vix) Gyro Bubble Level Menu for gyros (could be good for dynamic balancing)
x) Added support for Spektrum (R) Satellite and clones (See Note 1 below)
xi) 
xii) New, 8.32 maths library to accomodate high gyro rates for self level
xiii) 
xiv) Error check for "no motor layout" on Safe screen.
xv) Better handling of receiver signal loss.  Throttle drops to zero on roll or pitch signal loss to stop sudden flipping.
xvi) Limited camstab offset to 0 through 100 as this is percentage travel
xvii) 
xviii)
xix)
xx) 
xxi) Added arming test to recevier test menu.  Tells you when the yaw and throttle stick positions will arm or disarm.
xx) Minimised jitter for servos connected to M7 & M8 (thanks for RC911).  
xxi) Added a new Motor Layout called "Tricopter Servo M7".

Changes in operation:

i)  Defaults to AUX for Self Level On/Off
ii) All mixing resets to zero when you do a factory reset so you have to select a motor layout

Note 1 (Many thanks to David Thompson of OpenAero(2) fame for this feature)

Supports Spektrum(R) satellite with Tarot cable
Tested with Spektrum AR7/8000 DSM2 satellite and Orange R100 Satellite
Causes jitter on servos so not recommended for gimbal control or tricopters (etc)
Does not work with DSMX R110X satellite - if someone wants to send me one, I'll add it
Only supports 10 bit with all data in 1 frame 
Only supports 7 channels
Uses Throttle input for Tarot cable
Hold buttons 2&3 on power up to enter binding mode
If you switch between CPPM, Sat or normal receivers, power cycle the KK2.1
Debug lists 16 frame bytes from satellite - you'll know if there are 2 frames of data (so bind again).
KK2.1 Settings: -
You will need to set "Sat or CPPM" to "Yes" in Mode Settings
You will need to assign the channels correctly in Sat-CPPM Channels as  A=2,E=3,T=1,R=4,Aux=5
If using TX for gimbal control, CH6 & Ch7 are assigned

